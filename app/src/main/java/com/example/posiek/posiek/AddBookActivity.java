package com.example.posiek.posiek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddBookActivity extends AppCompatActivity {

    TextView textViewAuthorName;
    TextView textViewAuthorLastname;
    Spinner spinnerGenre;
    EditText editTextBookName;
    SeekBar seekBarRating;
    Button buttonAddBook;
    ListView listViewBooks;

    DatabaseReference databaseBooks;

    List<Book> books;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);

        buttonAddBook = (Button) findViewById(R.id.buttonAddBook);
        textViewAuthorName = (TextView) findViewById(R.id.textViewAuthorName);
        textViewAuthorLastname = (TextView) findViewById(R.id.textViewAuthorLastname);
        spinnerGenre = (Spinner) findViewById(R.id.spinnerGenre);
        editTextBookName =(EditText) findViewById(R.id.editTextBookName);
        seekBarRating = (SeekBar) findViewById(R.id.seekBarRating);
        listViewBooks = (ListView) findViewById(R.id.listViewBooks);

        Intent intent = getIntent();

        books = new ArrayList<>();
        String id = intent.getStringExtra(MainActivity.AUTHOR_ID);
        String name = intent.getStringExtra(MainActivity.AUTHOR_NAME);
        String lastname = intent.getStringExtra(MainActivity.AUTHOR_LASTNAME);


        textViewAuthorName.setText(name);
        textViewAuthorLastname.setText(lastname);


        databaseBooks = FirebaseDatabase.getInstance().getReference("books").child(id);

        buttonAddBook.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view){
            saveBook();
        }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseBooks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                books.clear();

                for(DataSnapshot bookSnapshot : dataSnapshot.getChildren()){
                    Book book = bookSnapshot.getValue(Book.class);
                    books.add(book);
                }

                BookList bookListAdapter = new BookList(AddBookActivity.this, books);
                listViewBooks.setAdapter(bookListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveBook(){
        String bookName = editTextBookName.getText().toString().trim();
        int rating = seekBarRating.getProgress();
        String genre = spinnerGenre.getSelectedItem().toString();
        if(!TextUtils.isEmpty(bookName)){
            String id = databaseBooks.push().getKey();

            Book book = new Book(id, bookName, rating, genre);

            databaseBooks.child(id).setValue(book);

            Toast.makeText(this, "Udało się zapisać książkę", Toast.LENGTH_LONG).show();

        }else{

        } Toast.makeText(this, "Pole nie może być puste", Toast.LENGTH_LONG).show();

    }
}
