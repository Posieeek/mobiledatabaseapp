package com.example.posiek.posiek;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String AUTHOR_NAME = "authorname";
    public static final String AUTHOR_LASTNAME = "authorlastname";
    public static final String AUTHOR_ID = "authorid";

    EditText editTextName;
    EditText editTextLastname;
    Button buttonAdd;
    Spinner spinnerGender;
    ListView listViewAuthors;
    List<Author> authors;
    DatabaseReference databaseAuthors;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
      //  FirebaseDatabase database = FirebaseDatabase.getInstance();
       // DatabaseReference myRef = database.getReference("message");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseAuthors = FirebaseDatabase.getInstance().getReference("authors");

        editTextName =(EditText) findViewById(R.id.editTextName);
        editTextLastname =(EditText) findViewById(R.id.editTextLastname);
        buttonAdd = (Button) findViewById(R.id.buttonAddAuthor);
        spinnerGender = (Spinner) findViewById(R.id.spinnerGender);

        listViewAuthors = (ListView) findViewById(R.id.listViewAuthors);

        authors = new ArrayList<>();

        buttonAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
            addAuthor();
            }
        });

        listViewAuthors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Author author = authors.get(i);

                Intent intent = new Intent(getApplicationContext(), AddBookActivity.class);
                intent.putExtra(AUTHOR_ID, author.getAuthorId());
                intent.putExtra(AUTHOR_NAME, author.getAuthorName());
                intent.putExtra(AUTHOR_LASTNAME, author.getAuthorLastname());

                startActivity(intent);
            }
        });

        listViewAuthors.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Author author = authors.get(i);

                showUpdateDialog(author.getAuthorId(), author.getAuthorName());
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseAuthors.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                authors.clear();
                for (DataSnapshot authorSnapshot : dataSnapshot.getChildren()) {
                    Author author = authorSnapshot.getValue(Author.class);

                    authors.add(author);
                }

                AuthorList adapter = new AuthorList(MainActivity.this, authors);
                listViewAuthors.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showUpdateDialog(final String authorId, String authorName){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.update_dialog, null);

        dialogBuilder.setView(dialogView);


        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText editTextLastname = (EditText) dialogView.findViewById(R.id.editTextLastname);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdate);
        final Spinner spinnerGenders = (Spinner) dialogView.findViewById(R.id.spinnerGenders);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDelete);


        dialogBuilder.setTitle("Aktualizowanie autora"+authorName);

      final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        buttonUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
            String name = editTextName.getText().toString().trim();
            String lastname = editTextLastname.getText().toString();
            String gender = spinnerGenders.getSelectedItem().toString();

            if(TextUtils.isEmpty(name)){
                editTextName.setError("Imię wymagane");
                return;
            }
            updateAuthor(authorId, name, lastname, gender);
            alertDialog.dismiss();


            }
        });

            buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteAuthor(authorId);

                }
            });
    }

    private void deleteAuthor(String authorId){
        DatabaseReference drAuthor = FirebaseDatabase.getInstance().getReference("authors").child(authorId);
        DatabaseReference drBooks = FirebaseDatabase.getInstance().getReference("books").child(authorId);

        drAuthor.removeValue();
        drBooks.removeValue();

        Toast.makeText(this, "Autor jest usunięty", Toast.LENGTH_LONG).show();
     }

    private boolean updateAuthor(String id, String name, String lastname, String gender){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("authors").child(id);
        Author author = new Author(id, name, lastname, gender);
        databaseReference.setValue(author);
        Toast.makeText(this, "Zaktualziowano autora", Toast.LENGTH_LONG).show();
        return true;
    }

    private  void addAuthor(){
        String name = editTextName.getText().toString().trim();
        String lastname = editTextLastname.getText().toString().trim();
        String gender = spinnerGender.getSelectedItem().toString();
        if(!TextUtils.isEmpty(name )){

          String id =  databaseAuthors.push().getKey();

         Author author = new Author(id, name, lastname, gender);

          databaseAuthors.child(id).setValue(author);

          Toast.makeText(this, "Dodano autora/autorkę!", Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(this, "Powinieneś wprowadzić imię", Toast.LENGTH_LONG).show();
        }
    }
}
