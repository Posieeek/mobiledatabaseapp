package com.example.posiek.posiek;

import android.widget.Spinner;

public class Book {
    private String bookId;
    private String bookName;
    private int bookRating;
    private String bookGenre;

    public Book(){

    }

    public Book(String bookId, String bookName, int bookRating, String bookGenre) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookRating = bookRating;
        this.bookGenre = bookGenre;
    }

    public String getBookId() {
        return bookId;
    }

    public String getBookGenre()
    {
        return bookGenre;
    }

    public String getBookName() {
        return bookName;
    }

    public int getBookRating() {
        return bookRating;
    }
}

