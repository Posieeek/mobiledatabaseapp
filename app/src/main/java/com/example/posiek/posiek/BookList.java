package com.example.posiek.posiek;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class BookList extends ArrayAdapter<Book> {
    private Activity context;
    private List<Book> books;

    public BookList(Activity context, List<Book> books) {
        super(context, R.layout.list_layout, books);
        this.context = context;
        this.books = books;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,  @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_book_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewRating = (TextView) listViewItem.findViewById(R.id.textViewRating);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.textViewGenre);

        Book book = books.get(position);
        textViewName.setText(book.getBookName());
        textViewGenre.setText(book.getBookGenre());
        textViewRating.setText(String.valueOf(book.getBookRating()));

        return listViewItem;
    }
}