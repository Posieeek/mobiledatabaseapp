package com.example.posiek.posiek;

public class Author {
  private String authorId;
  private  String authorName;
  private  String authorGender;
  private  String authorLastname;



    public Author()
    {

    }

    public Author(String authorId, String authorName, String authorLastname, String authorGender) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.authorLastname = authorLastname;
        this.authorGender = authorGender;

    }

    public String getAuthorId() {
        return authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorLastname() {
        return authorLastname;
    }

    public String getAuthorGender() {
        return authorGender;
    }


}